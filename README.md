﻿**HolidayApp**
========

HolidayApp jest to aplikacja mająca na celu wspmóc planowanie wycieczek. 


**Features**
--------

- Wpisywanie planów urlopowych z podpowiadaniem miejsc docelowych,
- Wizualizacja za pomocą markerów atrakcji,
- Wyświetlanie w panelu bocznym zdjęć atrakcji,
- Interfejs REST,
- Podpowiadanie atrakcyjnych miejsc w pobliżu miejsca docelowego

**Instalacja**
------------

Do porpawnego działania aplikacji niebędny jest serwer oraz baza danych. Aplikacja była tworzona na serwerze Tomcat. Do projektu została wykorzystana 
baza Apache Derby.

Aby pobrać projekt należy wpisać:
> **git clone [https://gitlab.com/milosz.cichy/HolidayApp.git](https://gitlab.com/milosz.cichy/HolidayApp.git)**

**Technologie**
-------

- Springa Data JPA 
- REST 
- Hibernate
- jQuery

**Diagram**
-------
![diagram](http://static.pokazywarka.pl/5/0/h/71fce91fb65e8af07702a90fdfc5d508_orig.jpg)

**Sposób działania aplikacji**
-------
Użytkownik wchodząc na strone zostanie poproszony o zakceptowanie zgodny pobranie lokalizacji. Loklizacja użytkownika będzie wprowadzona w pole początku podrózy. Użytkownik może zmienić te pole. Drugim pole, które musi wypełnic jest pole celu podrózy. Po wypelnieniu pierwszego formularzu użytkownik zostanie przeniosiony na ekrany mapy w zadeklarowanym położeniu. W obszarze w okół celu zostaną wyświetlone atrakcje w postaci markerów. Dostępny jest również podgląd atrakcji w menu bocznym. Aplikacja pozwala na wyznaczenie tras podróży. Kolejną dostępną usługą jest wyświetlanie hoteli w pobliżu 

**Dalsze plany**
-------
+ Obliczanie kosztów transportu wynikających z rodzaju transportu (np. ceny paliwa czy opłaty za autostrady przy podróży samochodem)
+ Sugerowanie kosztów noclegów w wybranym miejscu w oparciu o serwis zewnętrzny 
+ Sugerowanie zmiany terminów w oparciu o imprezy cykliczne w wybranym miejscu docelowym