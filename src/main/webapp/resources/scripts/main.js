var globalVariables = {
    map: null,
    destinationLat: 0,
    destinationLng: 0,
    locationLat: 0,
    locationLng: 0,
    destinationName: "",
    locationName: ""
};
$(document).ready(function () {

    if (window.performance.navigation.type === 2) {
        window.location.reload();
    }

    initGeo();

    if (getCookie("geo") === "") {
        getLocation();
   } else {
       $("#loc").val(getCookie("geo"));
   }

    function initGeo() {
        var dest = document.getElementById('des');
        var loc = document.getElementById('loc');
        var options = {
            types: [],
        };
        var autocomplete = new google.maps.places.Autocomplete(dest, options);
        var autocomplete = new google.maps.places.Autocomplete(loc, options);
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $("#geoSmall").html("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        $.ajax({
            dataType: 'json',
            type: "GET",
            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude +
            "," + position.coords.longitude + "&key=AIzaSyDA1imOG2uvEAvL89HrJDDT39LfTHjGjzA",
            success: function (result) {
                var geo = result.results[3].formatted_address;
                $("#loc").val(geo);
                document.cookie = "geo =" + geo + ";";
            },
            error: function () {
                $("#geoSmall").html("Geolocation failed");
            }
        });
        document.cookie = "lat =" + position.coords.latitude + ";";
        document.cookie = "lng =" + position.coords.longitude + ";";
        sessionStorage.setItem('locLat', position.coords.latitude);
        sessionStorage.setItem('locLng', position.coords.longitude);
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function getResolvedDest() {
        //  var destination = new Array();
        var loc = $("#loc").val();
        var des = $("#des").val();
        sessionStorage.setItem('locName', loc);
        sessionStorage.setItem('destName', des);
        $.ajax({
            type: "GET",
            url: "https://maps.googleapis.com/maps/api/geocode/json?address=" +
            des + "&key=AIzaSyDA1imOG2uvEAvL89HrJDDT39LfTHjGjzA",
            dataType: "json",
            success: function (result) {
                sessionStorage.setItem('destLat', result.results[0].geometry.location.lat);
                sessionStorage.setItem('destLng', result.results[0].geometry.location.lng);

                // window['globalVariables'].destinationLat = result.results[0].geometry.location.lat;
                // window['globalVariables'].destinationLng = result.results[0].geometry.location.lng;
                $.ajax({
                    type: "POST",
                    url: "/api/plan",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        startLat: getCookie("lat"),
                        startLng: getCookie("lng"),
                        place: {
                            "lat": result.results[0].geometry.location.lat,
                            "lng": result.results[0].geometry.location.lng,
                            "attraction": null,
                            "location": loc,
                            "destination": des
                        }
                    }),
                    success: function () {
                        console.log("POST Success");
                        window.location = "http://localhost:9090/pages/map.html"
                    },
                    error: function () {
                        console.log("Error during POST");
                    }
                });
                // document.cookie = "destLat=" + result.results[0].geometry.location.lat + ";";
                // document.cookie = "destLng=" + result.results[0].geometry.location.lng + ";";

                // return destination;
            }, error: function (result) {

                // globalVariables.destinationLat = result.results[0].geometry.location.lat;
                // globalVariables.destinationLng = result.results[0].geometry.location.lng;
                //window.location.reload(true);
            }
        });

    }

    $('#sbmBtn').on('click', function () {
        var loc = $("#loc").val();
        var des = $("#des").val();
        getResolvedDest();


    })
});