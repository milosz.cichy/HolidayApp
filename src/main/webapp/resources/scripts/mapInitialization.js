var directionsService;
var directionsDisplay;
var placesService;
var map;
var thisPlace;
var places;
var initMap = function (lat, lng) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(Number(sessionStorage.getItem('destLat')), Number(sessionStorage.getItem('destLng'))),
        zoom: 16,
        mapTypeControl: true,
        panControl: false,
        zoomControl: true,
        streetViewControl: false
    });
    thisPlace = map.center;
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    placesService = new google.maps.places.PlacesService(map);
    directionsDisplay.setMap(map);
    initGeo();
    findPlaces(5000, 'amusement_park', $("#attractions"));
}
var moveToLocation = function (lat, lng) {
    var center = new google.maps.LatLng(lat, lng);
    map.panTo(center);
}
var calculateAndDisplayRoute = function () {
    directionsService.route({
        origin: sessionStorage.getItem('locName'),
        destination: sessionStorage.getItem('destName'),
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}
var findPlaces = function (radius, type, htmlLoc) {
    var attr = $("#attractions");
    if (htmlLoc[0] === attr[0]) {
        htmlLoc.empty();
    } else {
        htmlLoc.find('ul').empty();
    }
    var request = {
        location: thisPlace,
        type: [type],
        radius: radius

    };
    placesService.textSearch(request, function (results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                places = results;
                var place = results[i];
                createMarker(place);
                htmlLoc.append("<ul style='display: block; color:orange; margin-left: 15px' id='" + i + "'>" + place.name + "</ul>");
                if (place.hasOwnProperty('photos') && place.photos.length > 0) {
                    htmlLoc.children(":last").append("<li> <img src='" + place.photos[0].getUrl({
                        'maxWidth': 75,
                        'maxHeight': 50
                    }) + "'/>" + "</li>");
                    htmlLoc.children(":last").find('li').toggle();
                }
                if (htmlLoc[0] === attr[0]) {
                    var atraction = {
                        "name": place.name,
                        "cost": 150
                    }
                    $.ajax({
                        type: "POST",
                        url: "/api/attraction",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(atraction),
                        success: function () {
                            console.log(atraction.cost + atraction.name);
                        },
                        error: function () {
                            console.log("Error during POST");
                        }
                    });
                }
            }

        }
    });
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var infowindow;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });
    if (place.hasOwnProperty('photos') && place.photos.length > 0) {
        infowindow = new google.maps.InfoWindow({
            content: "<div style='float:left'><li> <img src='" + place.photos[0].getUrl({
                'maxWidth': 75,
                'maxHeight': 50
            }) + "'/>" + "</div><div style='float:right; padding: 10px;'><b>" + place.name + "</b><br/>" + place.formatted_address + "</div>"
        });
    } else {
        infowindow = new google.maps.InfoWindow({
            content: "<div style='float:right; padding: 10px;'><b>" + place.name + "</b><br/>" + place.formatted_address + "</div>"
        });
    }
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, this);
    });
}

$(function () {
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');

    });
    // Remove menu for searching
    $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').removeClass('slide-in');

    });

});

function initGeo() {
    var dest = document.getElementById('des');
    var loc = document.getElementById('loc');
    var options = {
        types: [],
    };
    var autocomplete = new google.maps.places.Autocomplete(dest, options);
    var autocomplete = new google.maps.places.Autocomplete(loc, options);
}

$(document).ready(function () {
    $('#Hotel').on('click', function () {
        findPlaces(5000, 'hotels', $(this));
    })
    $('#attractions').on('click', 'ul', function (ev) {
        $('li', this).toggle(200);
    });
    $('#loc').val(sessionStorage.getItem('locName'));
    $('#des').val(sessionStorage.getItem('destName'));
    $('#dir').on('click', function () {
        calculateAndDisplayRoute();
    });

    $("#attractions").on('click', 'ul li', function () {
        var id = $(this).parent().get(0).id;
        var thisPlaceId = places[id];
        var thisPlaceLoc = thisPlaceId.geometry.location;
        map.zoom = 17;
        moveToLocation(thisPlaceLoc.lat(), thisPlaceLoc.lng());
    });
    $('#sbmBtn').on('click', function () {
        sessionStorage.setItem('locName', $('#loc').val());
        sessionStorage.setItem('destName', $('#des').val());

        $.ajax({
            type: "GET",
            url: "https://maps.googleapis.com/maps/api/geocode/json?address=" +
            $('#des').val() + "&key=AIzaSyDA1imOG2uvEAvL89HrJDDT39LfTHjGjzA",
            dataType: "json",
            success: function (result) {
                sessionStorage.setItem('destLat', result.results[0].geometry.location.lat);
                sessionStorage.setItem('destLng', result.results[0].geometry.location.lng);
                thisPlace = new google.maps.LatLng(Number(sessionStorage.getItem('destLat')), Number(sessionStorage.getItem('destLng')));
                calculateAndDisplayRoute();
                findPlaces(5000, 'amusement_park', $("#attractions"));
            }, error: function () {

            }
        });

    });


});