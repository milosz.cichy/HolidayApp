package controllers;

import db.Place;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dest")
public class MainController {
    private static final String CONTENT = "application/json";

    @RequestMapping(method = RequestMethod.POST, consumes = CONTENT)
    public void post(@RequestBody Place place) {
        System.out.println(place.toString());
    }
    @RequestMapping(method = RequestMethod.GET, produces = CONTENT)
    public Place get() {
        return new Place();
    }
}
