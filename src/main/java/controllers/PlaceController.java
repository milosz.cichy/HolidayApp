package controllers;

import db.Place;
import db.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/place")
public class PlaceController {



    @Autowired
    private PlaceRepository placeRepository;
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Place> getAllPlaces() {
        return placeRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public Place savePlace(@RequestBody Place place) {
        placeRepository.save(place);
        return place;
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public Place updatePlace(@RequestBody Place place) {
        placeRepository.save(place);
        return place;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Place deletePlace(@PathVariable("id") Integer id) {
        Place place = placeRepository.findById(id).get();
        placeRepository.delete(place);
        return place;

    }
}
