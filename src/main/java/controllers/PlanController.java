package controllers;

import db.Place;
import db.Plan;
import db.repository.PlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plan")
public class PlanController {


    @Autowired
    private PlanRepository planRepository;
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Plan> getAllPlans() {
        return planRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public Plan savePlan(@RequestBody Plan attraction) {
        planRepository.save(attraction);
        return attraction;
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public Plan updatePlan(@RequestBody Plan attraction) {
        planRepository.save(attraction);
        return attraction;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Plan deletePlan(@PathVariable("id") Integer id) {
        Plan plan = planRepository.findById(id).get();
        planRepository.delete(plan);
        return plan;

    }
    }


