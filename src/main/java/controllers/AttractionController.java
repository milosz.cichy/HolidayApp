package controllers;

import db.Attraction;
import db.Place;
import db.repository.AttractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/attraction")
public class AttractionController {

    @Autowired
    private AttractionRepository attractionRepository;
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Attraction> getAllAttractions() {
        return attractionRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public Attraction saveAttraction(@RequestBody Attraction attraction) {
        attractionRepository.save(attraction);
        return attraction;
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public Attraction updatePlan(@RequestBody Attraction attraction) {
        attractionRepository.save(attraction);
        return attraction;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Attraction deletePlan(@PathVariable("id") Integer id) {
        Attraction attraction = attractionRepository.findById(id).get();
        attractionRepository.delete(attraction);
        return attraction;

    }
}
