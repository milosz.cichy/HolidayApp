package db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Attraction {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    private double cost;
}
