package db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Plan {

    @Id
    @GeneratedValue
    private Integer id;
    private double budget;

    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    private Date endDate;
    private TypeOfTransport typeOfTransport;
    private double costOfTransport;
    private double startLat;
    private double startLng;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Place place;




}
