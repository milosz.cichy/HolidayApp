package db;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;

import java.util.Collection;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Place {

    @Id
    @GeneratedValue
    private Integer id;
    private double lat;
    private double lng;

    private String Destination;
    private String Location;


    @OneToMany(fetch = FetchType.EAGER)
    private Collection<Attraction> attraction;



}
