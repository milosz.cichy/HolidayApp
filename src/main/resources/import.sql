
INSERT INTO ATTRACTION(id,name,cost) VALUES (3,'Wytwórnia Figur',550);
INSERT INTO ATTRACTION(id,name,cost) VALUES (4,'Wytwórnia Figur',550);

INSERT INTO PLACE(ID,DESTINATION,LOCATION,LAT,LNG) VALUES (1,'Opole','Wrocław',53,18);
INSERT INTO PLACE(ID,DESTINATION,LOCATION,LAT,LNG) VALUES (2,'Poznań','Kraków',53,18);


INSERT INTO PLAN(ID,BUDGET,COSTOFTRANSPORT,ENDDATE,STARTDATE,STARTLAT,STARTLNG,TYPEOFTRANSPORT,PLACE_ID) VALUES (1,300,100,'2011-03-12' ,'2011-03-12',52,18,1,2);
INSERT INTO PLAN(ID,BUDGET,COSTOFTRANSPORT,ENDDATE,STARTDATE,STARTLAT,STARTLNG,TYPEOFTRANSPORT,PLACE_ID) VALUES (2,155,80,'2018-03-18' ,'2018-03-12',52,18,2,2);


INSERT INTO PLACE_ATTRACTION(PLACE_ID,ATTRACTION_ID) VALUES (1,3);
INSERT INTO PLACE_ATTRACTION(PLACE_ID,ATTRACTION_ID) VALUES (1,4);
